**1.1.1 (2016-07-01)**

* Added alternative hreflang links to HTTP header for the splash page
* Added href-tags element that generates the href language tags
* Change the minimum-stability to stable


**1.1.0 (2016-06-06)**

* Updated composer.json, to match new tags in plugins and also updated CakePHP tag
* Added domain redirection to bootstrap settings

**1.0.10 (2016-04-05)**

* Add flare to bower.json

**1.0.9 (2016-03-08)**

* Allow Composer to download from http url's

**1.0.8 (2016-03-08)**

* Added .user.ini in public_html folder with NewRelic config

**1.0.7 (2016-03-02)**

* Fix plugin formats override
* add equal-height bower component

**1.0.6 (2016-03-01)**

* Update default mail layout
* Add standard touch icons
* Added standard scss files
* Add standard mixins
* Add default print css
* Add dummy og image
* Move analytics element to head
* Add default phpThumb format into bootstrap config file
* Remove files folder from robots.txt file

**1.0.5 (2016-02-18)**

* Throw error when changing branch, while gulp is still running
* Ignore pot files in git

**1.0.4 (2016-02-11)**

* Fixed browser sync so port gets increased automatically


**1.0.3 (2016-02-10)**

* Added CakeLog config to skeleton bootstrap.php as the are no longer auto-configured by cakephp

**1.0.2 (2016-02-05)**

* Remove memcache as default caching engine
* Allow creation of multiple css files by adding subdirectories in the Stylesheets directory
* Recursive copy images and fonts to .tmp directory, so paths in css files match when viewing page via browsersync.

**1.0.1 (2016-01-12)**

* Removed php short tag line in .htaccess
