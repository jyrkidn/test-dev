var gulp = require('gulp'),
    path = require('path'),
    uglifycss = require('gulp-uglifycss'),
    browserSync = require('browser-sync').create(),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    wiredep = require('wiredep').stream,
    gulpFilter = require('gulp-filter'),
    mainBowerFiles = require('main-bower-files'),
    merge = require('merge-stream'),
    inject = require('gulp-inject'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
    gutil = require('gulp-util'),
    del = require('del'),
    fs = require('fs'),
    assets,
    destPath = 'public_html';

require('es6-promise').polyfill();

assets = {
    styles: 'app/Assets/Stylesheets/',
    scripts: 'app/Assets/Scripts/',
    fonts: 'app/Assets/Fonts/**/*'
}

function getFolders(dir) {
    return fs.readdirSync(dir)
        .filter(function(file) {
            return fs.statSync(path.join(dir, file)).isDirectory();
        });
}

gulp.task('styles', function() {
    var folders = getFolders(assets.styles),
        tasks,
        sassOptions = {
            style: 'expanded'
        },
        injectFiles = gulp.src([
            path.join(assets.styles, '/*.scss'),
            path.join('!' + assets.styles, '/website.scss')
        ], { read: false })
        injectOptions = {
            transform: function(filePath) {
                filePath = filePath.replace(assets.styles, '');
                return '@import "' + filePath + '";';
            },
            starttag: '// injector',
            endtag: '// endinjector',
            addRootSlash: false
        };

    tasks = folders.map(function(folder) {
        var name = folder.toLowerCase();

        return gulp.src([
                path.join(assets.styles, folder, '/', name + '.scss')
            ])
            .pipe(sourcemaps.init())
            .pipe(sass(sassOptions).on('error', sass.logError))
            .pipe(autoprefixer())
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(path.join(destPath, '/.tmp/styles/')))
            .pipe(uglifycss({
                "max-line-len": 80
            }))
            .pipe(gulp.dest(path.join(destPath, '/css/')))
            .pipe(browserSync.stream());
    });

    root = gulp.src([
            path.join(assets.styles, '/website.scss')
        ])
        .pipe(inject(injectFiles, injectOptions))
        .pipe(wiredep({
            directory: 'app/bower_components',
            bowerJson: require('./app/bower.json')
        }))
        .pipe(sourcemaps.init())
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.join(destPath, '/.tmp/styles/')))
        .pipe(uglifycss({
            "max-line-len": 80
        }))
        .pipe(gulp.dest(path.join(destPath, '/css/')))
        .pipe(browserSync.stream());

    return merge(tasks, root);
});

// Lint JS
gulp.task('jshint', function () {
    return gulp.src(assets.scripts + '**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jshint.reporter('fail'));
});

// Concat & Minify JS
gulp.task('scripts', ['jshint'], function() {
    var folders = getFolders(assets.scripts),
        tasks,
        root;

    tasks = folders.map(function(folder) {
        var name = folder.toLowerCase();

        return gulp.src(path.join(assets.scripts, folder, '/**/*.js'))
            .pipe(sourcemaps.init())
            .pipe(concat(name + '.js'))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest('public_html/.tmp/scripts/'))
            .pipe(uglify())
            .pipe(gulp.dest('public_html/js'));
    });

    // root = gulp.src(['app/Plugin/*/Assets/Scripts/**/*.js', '!app/Plugin/CakeAdmin/Assets/Scripts/**/*.js', assets.scripts + '*.js'])
    root = gulp.src(assets.scripts + '*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('website.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('public_html/.tmp/scripts/'))
        .pipe(uglify())
        .pipe(gulp.dest('public_html/js'));

   return merge(tasks, root);
});

gulp.task('wiredep', function () {
    var jsFilter = gulpFilter('*.js', {restore: true}),
        cssFilter = gulpFilter('**/*.css', {restore: true});

    return gulp.src(mainBowerFiles({
        paths: {
            bowerDirectory: 'app/bower_components',
            bowerrc: 'app/.bowerrc',
            bowerJson: 'app/bower.json'
        }
    }))

        // grab vendor js files from bower_components, minify and push in /public
        .pipe(jsFilter)
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(destPath + '/.tmp/scripts'))
        .pipe(uglify())
        .pipe(gulp.dest(destPath + '/js'))
        .pipe(jsFilter.restore)

        // grab vendor css files from bower_components, minify and push in /public
        .pipe(cssFilter)
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest(destPath + '/.tmp/styles'))
        .pipe(uglifycss())
        .pipe(gulp.dest(destPath + '/css'))
        .pipe(cssFilter.restore);
});

gulp.task('fonts', function () {
    return gulp.src(mainBowerFiles({
        filter: '**/*.{eot,svg,ttf,woff,woff2}',
        paths: {
            bowerDirectory: 'app/bower_components',
            bowerrc: 'app/.bowerrc',
            bowerJson: 'app/bower.json'
        }
    }).concat(assets.fonts))
        .pipe(gulp.dest(destPath + '/.tmp/fonts'))
        .pipe(gulp.dest(destPath + '/fonts'));
});

gulp.task('clean', function (cb) {
    del([
        destPath + '/.tmp/*',
        destPath + '/js/*',
        '!' + destPath + '/js/empty',
        destPath + '/css/*',
        '!' + destPath + '/css/empty',
        destPath + '/fonts/*',
        '!' + destPath + '/fonts/empty',
    ], cb);
});

gulp.task('watch', ['wiredep', 'fonts', 'scripts', 'styles', 'tmpCopy'], function() {
    browserSync.init({
        proxy: __dirname.replace('/srv', 'hq.codedor.be') + '/public_html/',
        host: 'hq.codedor.be',
        port: 4725,
        ui: false,
        open: false,
        rewriteRules: [
            {
                match: /\/www\/public_html\/css\//g,
                fn: function (match) {
                    return '/www/public_html/.tmp/styles/';
                }
            },
            {
                match: /\/www\/public_html\/js\//g,
                fn: function (match) {
                    return '/www/public_html/.tmp/scripts/';
                }
            }
        ]
    });

    gulp.watch('.git/HEAD', function() {
        throw new gutil.PluginError('git-branch', {
            message: 'You changed git branch!'
        });
    });

    // Recompile less when a less file was changed
    gulp.watch(assets.styles + '**/*', ['styles']);

    // Watch JS Files
    gulp.watch(assets.scripts + '**/*.js', ['scripts']).on('change', browserSync.reload);

    // Watch fonts folder
    gulp.watch(assets.fonts + '**/*', ['fonts']).on('change', browserSync.reload);

    // Reload browser and update vendor when a bower package has been updated or added
    gulp.watch('app/bower.json', ['wiredep']).on('change', browserSync.reload);
});


gulp.task('build', ['clean'], function(cb) {
    // Only start watching if clean task has finished
    gulp.start('watch');

    // Add php and ctp watch here, because it breaks if we do this in the watch task
    gulp.watch('app/**/*.{php,ctp}').on('change', browserSync.reload);
});

// Copy fonts and images
gulp.task('tmpCopy', function() {
    var fonts = gulp.src(['fonts/**/*'], {cwd: destPath})
        .pipe(gulp.dest(destPath + '/.tmp/fonts'));

    var img = gulp.src(['img/**/*'], {cwd: destPath})
        .pipe(gulp.dest(destPath + '/.tmp/img'));

    return merge(fonts, img);
});

gulp.task('init', ['clean'], function(cb) {
    gulp.start('wiredep');
    gulp.start('fonts');
    gulp.start('scripts');
    gulp.start('styles');
});

gulp.task('default', ['clean', 'build'], browserSync.reload);
