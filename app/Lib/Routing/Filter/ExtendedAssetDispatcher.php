<?php
App::uses('AssetDispatcher', 'Routing/Filter');

class ExtendedAssetDispatcher extends AssetDispatcher
{

    protected function _deliverAsset(CakeResponse $response, $assetFile, $ext)
    {
        ob_start();
        $compressionEnabled = Configure::read('Asset.compress') && $response->compress();
        if ($response->type($ext) === $ext) {
            $contentType = 'application/octet-stream';
            $agent       = env('HTTP_USER_AGENT');
            if (preg_match('%Opera(/| )([0-9].[0-9]{1,2})%', $agent) || preg_match('/MSIE ([0-9].[0-9]{1,2})/', $agent)) {
                $contentType = 'application/octetstream';
            }
            $response->type($contentType);
        }
        $response->length(false);
        $response->cache(filemtime($assetFile));
        $response->send();
        ob_clean();

        readfile($assetFile);

        if ($compressionEnabled) {
            ob_end_flush();
        }
    }
}
