#!/usr/bin/php -q
<?php
/**
 * Command-line code generation utility to automate programmer chores.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Console
 * @since         CakePHP(tm) v 2.0
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

define('DS', DIRECTORY_SEPARATOR);
define('CAKE_CORE_INCLUDE_PATH', dirname(dirname(__FILE__)) . DS . 'Vendor' . '/cakephp/cakephp/lib');
define('CAKEPHP_SHELL', true);
define('CORE_PATH', CAKE_CORE_INCLUDE_PATH . DS);

$dispatcher = CAKE_CORE_INCLUDE_PATH . '/Cake/Console/ShellDispatcher.php';

if (!include(CAKE_CORE_INCLUDE_PATH . '/Cake/Console/ShellDispatcher.php')) {
    trigger_error('Could not locate CakePHP core files.', E_USER_ERROR);
}

return ShellDispatcher::run($argv);
