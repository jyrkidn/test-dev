<?php
/**
 * Static content controller.
 *
 * This file will render views from views/static/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

class StaticController extends AppController {

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Static';

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array();

    public function splash()
    {
        // Set alternate hreflang headers, so the splash page is an alternate url for our homepage for Google
        $headers = array();
        foreach (Language::all() as $locale => $language) {
            $url = Router::url(Resources::url('Home', null, array('locale' => $locale)), true);
            $headers[] = '<' . $url . '>; rel="alternate"; hreflang="' . $locale . '"';
        }
        $this->response->header('Link: ' . implode(', ', $headers));

        $locale = Language::locale();
        $this->redirect('/' . $locale);
    }

    public function home()
    {
        $this->layout = 'home';
    }

    public function switchLocale($locale, $url = null)
    {

        if (!$url) {
            $this->redirect("/$locale");
        }

        // Remove the parameters that are added by the transelateUrl
        // (This happens when in preview mode)
        $parts = explode('?', Resources::translateUrl($locale, $url));
        $base = $parts[0];

        $localeUrl = $base . Router::queryString($this->request->query);

        $this->redirect($localeUrl ?: "/$locale");
    }
}
