<h1><?php echo $name; ?></h1>

<p>
    <?php echo __('[default.error occured]'); ?>
</p>

<p>
    <?php echo __('[default.return to our homepage]'); ?>
</p>

<p>
    <?php echo Configure::read('Site.name'); ?>
</p>

<p>
    <?php echo $this->Html->link($this->Html->url('/'), '/'); ?>
</p>

<?php if (Configure::read('debug') > 0) : ?>
    <?php echo $this->element('exception_stack_trace'); ?>
<?php endif; ?>
