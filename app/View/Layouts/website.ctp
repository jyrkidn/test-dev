<?php $this->extend('default'); ?>

<?php $this->assign('title', $title_for_layout.' | '.Configure::read('Site.name')); ?>

<?php
echo $this->Menus->breadcrumbs('main',
    array(
    'home' => array(
        'name' => __d('general', '[general.home]'),
        'url' => Resources::url('Home'),
    )
));
?>

<?php echo $this->fetch('content'); ?>
