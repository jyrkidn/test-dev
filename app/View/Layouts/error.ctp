<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $error->getCode().' '.$name; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <meta name="description" content="">
    <meta name="author" content="" >
    <meta name="generator" content="Code d'Or bvba - https://codedor.be" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php echo $this->fetch('meta'); ?>

    <?php echo $this->Html->meta('icon'); ?>
    <link rel="icon" sizes="192x192" href="<?php echo $this->Html->url('/img/touch-icon.png', true);?>">
    <link rel="apple-touch-icon" href="<?php echo $this->Html->url('/img/touch-icon.png', true);?>">

    <?php
        echo $this->Html->css(array('website'), 'stylesheet', array('media' => 'screen'));
        echo $this->Html->css(array('print'), 'stylesheet', array('media' => 'print'));
    ?>
    <?php echo $this->fetch('css'); ?>
    <?php echo $this->element('Utils.analytics'); ?>
</head>

<body>
    <div class="container">
        <?php echo $this->fetch('content'); ?>

        <footer>
            <p>&copy; <?php echo date('Y'); ?> <?php echo Configure::read('Site.name'); ?></p>
        </footer>
    </div>
    <?php echo $this->fetch('script'); ?>
</body>
</html>
