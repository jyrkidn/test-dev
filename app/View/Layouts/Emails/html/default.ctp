<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <title><?php echo Configure::read('Site.name'); ?></title>
        <style type="text/css" media="screen">

            /* Client-specific Styles */
            #outlook a { padding: 0; }  /* Force Outlook to provide a "view in browser" button. */
            body { width: 100% !important; }
            .ReadMsgBody { width: 100%; }
            .ExternalClass { width: 100%; display:block !important; } /* Force Hotmail to display emails at full width */
            /* Reset Styles */
            /* Add 100px so mobile switch bar doesn't cover street address. */
            body { background-color: #eeeeee; margin: 0; padding: 0; }
            img { outline: none; text-decoration: none; display: block;}
            br, strong br, b br, em br, i br { line-height:100%; }

            table{
                font-size: 14px;
            }

            a img{
                border: none;
            }

            .body a{
                color: #000000;
                text-decoration: underline;
                word-break: break-word;
                word-wrap: break-word;
            }

            .body a.btn{
                display: inline-block;
                padding-left: 15px;
                padding-right: 15px;
                font-size: 14px;
                line-height: 30px;
                background: #1f1f1f;
                color: #ffffff;
                text-decoration: none;
            }

            .footer{
                color: #ffffff;
            }

            .footer a{
                color: inherit;
                text-decoration: none;
            }

            /* Mobile-specific Styles */
		    @media only screen and (max-width: 660px) {
			    .container{
                    width: 500px !important;
                }
		    }

            @media only screen and (max-width: 560px) {
			    .container{
                    width: 400px !important;
                }
		    }

            @media only screen and (max-width: 460px) {
			    .container{
                    width: 300px !important;
                }
		    }

        </style>
    </head>

    <body style="background: #efefef; margin: 0; padding:0; color:#3B3B3B; font-family: Arial, sans-serif; font-size: 14px; line-height: 24px;" >
        <table border="0" align="center" width="100%">
            <tr>
                <td height="50" style="font-size:1px; height: 50px;">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table width="640" class="container" style="background: #ffffff;" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <!-- Header -->
                                <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td colspan="3" height="20" style="font-size:1px; height: 20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="25" style="font-size:1px; width: 25px;">&nbsp;</td>
                                        <td>
                                            <?php
                                                echo $this->Html->link(
                                                    $this->Html->image(
                                                        $this->Html->url('/img/admin_logo.jpg', true),
                                                        array('border' => 'none', 'width' => 200, 'height' => 'auto')
                                                    ),
                                                    $this->Html->url(Resources::url('Home'), true),
                                                    array('escape' => false)
                                                );
                                            ?>
                                        </td>
                                        <td width="25" style="font-size:1px; width: 25px;">&nbsp;</td>
                                    </tr>
                                </table>
                                <!-- End of Header -->
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <!-- Body -->
                                <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0" style="font-size: 12px; line-height: 20px;" class="body">
                                    <tr>
                                        <td colspan="3" height="35" style="font-size:1px; height: 35px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="25" style="font-size:1px; width: 25px !important;">&nbsp;</td>
                                        <td>
                                            <?php echo $content_for_layout ?>
                                        </td>
                                        <td width="25" style="font-size:1px; width: 25px !important;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" height="50" style="font-size:1px; height: 50px;">&nbsp;</td>
                                    </tr>
                                </table>
                                <!-- End of Body -->
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <!-- Footer -->
                                <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0" class="footer" style="background: #000000;">
                                    <tr>
                                        <td colspan="3" height="15" style="font-size:1px; height: 15px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="15" style="font-size:1px; width: 15px;">&nbsp;</td>
                                        <td style="color: #ffffff;">
                                            <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0" class="footer" style="font-size: 12px; line-height: 22px;">
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <?php echo Configure::read('Site.name'); ?> | <a href="mailto:<?php echo Configure::read('Site.email'); ?>"><?php echo Configure::read('Site.email'); ?></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="15" style="font-size:1px; width: 15px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" height="15" style="font-size:1px; height: 15px;">&nbsp;</td>
                                    </tr>
                                </table>
                                <!-- End of Footer -->
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="50" style="font-size:1px; height: 50px;">&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
