<!DOCTYPE html>
<html lang="<?php echo Language::locale(); ?>">
    <head>
        <meta charset="utf-8">
        <title><?php echo strip_tags($this->fetch('title')); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <meta name="generator" content="Code d'Or bvba - https://codedor.be" >
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php echo $this->element('href_tags'); ?>
        <?php echo $this->fetch('meta'); ?>

        <?php echo $this->Html->meta('icon'); ?>
        <link rel="icon" sizes="192x192" href="<?php echo $this->Html->url('/img/touch-icon.png', true);?>">
        <link rel="apple-touch-icon" href="<?php echo $this->Html->url('/img/touch-icon.png', true);?>">

        <?php
            echo $this->Html->css(array('website'), 'stylesheet', array('media' => 'screen'));
            echo $this->Html->css(array('print'), 'stylesheet', array('media' => 'print'));
        ?>
        <?php echo $this->fetch('css'); ?>
        <?php echo $this->element('Utils.analytics'); ?>
    </head>

    <body>
        <?php if (isset($_GET['preview_id'], $_GET['preview_hash'])) : ?>
            <div class="row">
                <div class="alert alert-warning pagination-centered">
                    <?php echo __d('admin', 'You are previewing the website. All online & offline items are visible.'); ?>
                    <?php
                    echo __d('admin', 'You can %s.', $this->Html->link(__d('admin', 'switch to the regular view'), '/'));
                    ?>
                </div>
            </div>
        <?php endif; ?>

        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><?php echo Configure::read('Site.name'); ?></a>
            </div>

            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <?php
                echo $this->Menus->menu('main', array('openActive' => false, 'ul' => array('class' => 'nav navbar-nav')));
                ?>
            </div>
        </nav>

        <?php echo $this->element('lang_switcher'); ?>

        <div class="container">
            <?php echo $this->fetch('content'); ?>

            <footer>
                <p>&copy; <?php echo date('Y'); ?> <?php echo Configure::read('Site.name'); ?></p>
            </footer>
        </div>

        <script>
            Webroot = <?php echo json_encode(Router::url('/', true)); ?>;
        </script>

        <?php echo $this->Html->script('vendor'); ?>

        <?php echo $this->Html->script('website'); ?>
        <?php echo $this->fetch('script'); ?>
        <?php echo $this->fetch('scriptBottom'); ?>
    </body>
</html>
