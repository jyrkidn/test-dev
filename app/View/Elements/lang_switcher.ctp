<div class="langselect">
<?php
    $options = array();

    foreach (Language::all() as $locale => $language) {
        if ($language['online']) {
            $options[$locale] = $language['human'];
        }
    }

    // Select box
    $url = $this->request->url . Router::queryString($this->request->query);
    if (Language::locale()) {
        echo $this->Form->input('lang', array(
            'label' => false,
            'div' => false,
            'options' => $options,
            'selected' => Language::locale(),
            'id' => 'lang-switcher',
            'name' => 'lang',
            'data-url' => $this->webroot . 'switch-locale/{:locale}/' . $url,
        ));
    }
?>
	<ul>
		<?php foreach ($options as $locale => $language) : ?>
            <li<?php echo $locale == Language::locale() ? ' class="active"' : ''; ?>>
                <?php echo $this->Html->link($language, "/switch-locale/$locale/$url"); ?>
            </li>
        <?php endforeach; ?>
	</ul>
</div>