<?php

    $currentLocale = Language::locale();
    $currentUrl = strstr($this->Html->url(null), DS . $currentLocale);
    $parsedCurrentUrl = Router::parse($currentUrl);

    foreach (Language::all() as $locale => $language) {

        $url = Resources::translateUrl($locale, $currentUrl, true);

        $parsedUrl = Router::parse(strstr($this->Html->url($url), DS . $locale));
        Language::locale($currentLocale);

        if (isset($parsedCurrentUrl['controller']) && isset($parsedCurrentUrl['action']) && isset($parsedUrl['controller']) && isset($parsedUrl['action'])) {
            if ($parsedCurrentUrl['controller'] === $parsedUrl['controller'] && $parsedCurrentUrl['action'] === $parsedUrl['action']) {
                echo $this->Html->tag(
                    'link',
                    null,
                    array(
                        'href' => $url,
                        'rel' => 'alternate',
                        'hreflang' => $locale
                    )
                );
            }
        }

    }

    if ($parsedCurrentUrl['controller'] === 'static' && $parsedCurrentUrl['action'] === 'home') {
        echo $this->Html->tag(
            'link',
            null,
            array(
                'href' => Router::url('/', true),
                'rel' => 'alternate',
                'hreflang' => 'x-default'
            )
        );
    }

