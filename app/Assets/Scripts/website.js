$(document).ready(function() {
	$('#lang-switcher').on('change', function() {
		$this = $(this);
		var url = $this.attr('data-url');
		url = url.replace('{:locale}', this.value);
		window.location = url;
	});
});