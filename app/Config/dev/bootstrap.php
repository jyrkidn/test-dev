<?php

Configure::write('debug', 2);

// Files path
define('FILES', '_files');
define('FILES_ROOT', realpath(WWW_ROOT . '../../../' . FILES));

// Disable caching
Configure::write('Cache.disable', true);

// Set to true in order to disable the spider crawling
Configure::write('Spider.disable', false);

// Set the maximum amount of wget threads allowed per crawl
Configure::write('Spider.threads', 7);

// Set the maximum allowed load (above this load no other crawls will start)
Configure::write('Spider.loadMaximum', 97);

// Set to true in order to minify the html before sending it to the client
Configure::write('Minify.html', false);

// Set to true in order to force redirection over https
Configure::write('UrlSanitize.enableSSLRedirect', false);

// Set to true in order to disable main-domain force and allow traffic using www-subdomain
Configure::write('UrlSanitize.disableWwwRedirect', false);

// Set to true in order to disable sanitization of trailing slashes in the current url
Configure::write('UrlSanitize.disableTrailingSlashRedirect', false);

// Set to true in order to disable redirection of all traffic to domains listed below in the allowedDomains config
Configure::write('UrlSanitize.disableDomainRedirect', true);

// All domains allowed for this website (all subdomains have to be specified),
// Leave empty to allow all domains
Configure::write('UrlSanitize.allowedDomains', array());

//See http://10.20.1.1/redmine/issues/16736 for more info about this line
if (isset($_SERVER['REDIRECT_URL'])) {
    unset($_GET['/' . str_replace(str_replace('index.php', '', env('PHP_SELF')), '', $_SERVER['REDIRECT_URL'])]);
}
