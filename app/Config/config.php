<?php
$config = array(
    'Site' => array(
        'Site.name' => array(
            'field' => 'string',
        ),
        'Site.email' => array(
            'field' => 'string',
        ),
        'Site.googleAnalytics' => array(
            'field' => 'string',
        ),
        'Site.googleAnalyticsTesting' => array(
            'field' => array(
                        'label' => 'Google Analytics for Testing',
                        'type' => 'string'
                    )
        ),
        'Site.maintenance' => array(
            'field' => 'boolean',
            'description' => 'Take the site offline for maintenance.'
        ),
    ),
);
