<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Resources', 'System.Routing');

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

Router::connect('/', array('controller' => 'static', 'action' => 'splash'));

Resources::connect(
    'Home',
    array(
        'nl' => '',
        'fr' => '',
        'en' => '',
    ),
    array('controller' => 'static', 'action' => 'home')
);

Router::connect(
    '/switch-locale/:locale/**',
    array('controller' => 'static', 'action' => 'switchLocale'),
    array('pass' => array('locale'))
);

/**
 * Example of connecting a resource.
 */
//    Resources::connect('Post::view', array(
//        'nl' => '/berichten/:id',
//        'en' => '/post/:id',
//    ), array('controller' => 'posts', 'action' => 'view'));

/**
 * Static pages.
 */
//    Resources::connect('Static', array(
//        'nl' => '/static/:action/*',
//        'en' => '/static/:action/*',
//    ), array('controller' => 'static'));

Resources::connect(
    'Page::view',
    array(
        'nl' => '/:slug',
        'fr' => '/:slug',
        'en' => '/:slug',
    ),
    array('controller' => 'pages', 'action' => 'view', 'plugin' => 'Page')
);

/**
 * Load the CakePHP default routes. Remove this if you do not want to use
 * the built-in default routes.
 */
//    require CAKE . 'Config' . DS . 'routes.php';

Router::parseExtensions('rss', 'xml');
