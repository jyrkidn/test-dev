<?php
if (!include(Configure::read('env') . '/email.php')) {
    throw new RuntimeException(sprintf("Missing '%s'", Configure::read('env') . '/email.php'));
}
