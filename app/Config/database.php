<?php

if (!include(Configure::read('env') . '/database.php')) {
    throw new RuntimeException(sprintf("Missing '%s'", Configure::read('env') . '/database.php'));
}
