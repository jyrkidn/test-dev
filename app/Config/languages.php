<?php
    App::uses('Language', 'System.I18n');

    // Add languages
    Language::add(array(
        'locale' => 'nl',
        'short' => 'nld',
        'online' => true,
        'human' => 'Nederlands',
        'domains' => array(),
        'cookie' => 'none',
    ));

    Language::add(array(
        'locale' => 'fr',
        'short' => 'fra',
        'online' => true,
        'human' => 'Français',
        'domains' => array(),
        'cookie' => 'none',
    ));

    Language::add(array(
        'locale' => 'en',
        'short' => 'eng',
        'online' => true,
        'human' => 'English',
        'domains' => array(),
        'cookie' => 'none',
    ));

    Language::$defaultLocale = 'nl';
