<?php
namespace ComposerScripts;

use Composer\Installer\PackageEvent;

class PostPackageInstall
{
    
    public static function run(PackageEvent $event)
    {
        $package = $event->getOperation()->getPackage();
        $name = $package->getName();
        $commit = $package->getSourceReference();
        $extra = $package->getExtra();
        if ($package->getType() === 'cakephp-plugin') {
            if (isset($extra['installer-name'])) {
                $plugin = $extra['installer-name'];
            } else {
                $name_elements = explode('/', $name);
                $plugin = array_pop($name_elements);
            }
            passthru(sprintf('Console/cake System.Plugin InitConfig %s %s', $plugin, $commit));
        }
    }

}
