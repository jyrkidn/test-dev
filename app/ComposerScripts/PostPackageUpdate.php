<?php
namespace ComposerScripts;

use Composer\Installer\PackageEvent;

class PostPackageUpdate
{
    
    public static function run(PackageEvent $event)
    {
        $package = $event->getOperation()->getInitialPackage();
        $name = $package->getName();
        $extra = $package->getExtra();
        if ($package->getType() === 'cakephp-plugin') {
            if (isset($extra['installer-name'])) {
                $plugin = $extra['installer-name'];
            } else {
                $name_elements = explode('/', $name);
                $plugin = array_pop($name_elements);
            }
            passthru(sprintf('Console/cake System.Plugin UpdateConfig %s', $plugin));
        }
    }

}
